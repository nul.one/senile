#!/bin/bash
# -*- coding: UTF-8 -*-

set -e
script_dir=$(cd `dirname "$0"`; pwd; cd - 2>&1 >> /dev/null)
data_path=$script_dir/data_dir/senile_test_data
rm -rf $(dirname $data_path)

export SENILE_DB_PATH=$data_path
pip install pytest coverage pytest-cov black
pip install -U $script_dir/..
py.test -vv --cov=senile --cov-report html
coverage report
black $script_dir/../senile --check

rm -rf $(dirname $data_path)

