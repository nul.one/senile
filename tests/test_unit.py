
import pytest

from senile import todo, database, icons
from senile import __main__ as m
import os
import time

###
# Fixtures
###

TASK_DATA = [
        "task 1",
        "task 2 +tag_a",
        "task 3 +tag_b",
        "task 4 +tag_a +tag_b",
        "task 5 +tag_a -tag_b",
        "task 6 +tag_c +tag_d +tag_e -tag_e -tag_f",
        "task 7 %17 +tag_a",
        "task 8 +tag_a %18",
        "task 9 %19a %20 +tag_c",
        "task 10 @active %5 +tag_e",
        "task 11 @done +tag_e",
        "task 12 +tag_e @hidden",
        "task 13 @hidden @active more text %44 +tag_e",
        "task 14 @todo test %2 it +tag_c",
        ]
TAGS_COUNT = [
        (5,"tag_a"),
        (4,"tag_e"),
        (3,"tag_c"),
        (2,"tag_b"),
        (1,"tag_d"),
        ]
TASK_TABLE = """\
id | uuid     | tags        | text              | duration | p  | s 
===+==========+=============+===================+==========+====+===
 - | ce5729b3 | tag_e       | task 12           | 0:00:00  | 0  | 👻
11 | ce5729b2 | tag_e       | task 11           | 0:00:00  | 0  | ✔️ 
 1 | ce5729a8 |             | task 1            | 0:00:00  | 0  | ⭕
 3 | ce5729aa | tag_b       | task 3            | 0:00:00  | 0  | ⭕
 4 | ce5729ab | tag_a tag_b | task 4            | 0:00:00  | 0  | ⭕
 5 | ce5729ac | tag_a       | task 5            | 0:00:00  | 0  | ⭕
 6 | ce5729ad | tag_c tag_d | task 6            | 0:00:00  | 0  | ⭕
 2 | ce5729a9 | tag_a       | task 2            | 0:00:00  | 0  | ⭕
13 | ce5729b5 | tag_c       | task 14 test it   | 0:00:00  | 2  | ⭕
 7 | ce5729ae | tag_a       | task 7            | 0:00:00  | 17 | ⭕
 8 | ce5729af | tag_a       | task 8            | 0:00:00  | 18 | ⭕
 9 | ce5729b0 | tag_c       | task 9 %19a       | 0:00:00  | 20 | ⭕
10 | ce5729b1 | tag_e       | task 10           | 0:00:06  | 5  | 🔴
12 | ce5729b4 | tag_e       | task 13 more text | 0:00:06  | 44 | 🔴\
"""
TASK_2_NOTE = "Just a test note."
TASK_2_INFO = """\
+--------------------------------------------------+
|       id:   2                                    |
|     uuid:   acf7441d-c159-11e9-99ab-c0b6f9271fff |
|     text:   task 2                               |
| priority:   0                                    |
|   status:   todo ⭕                              |
|  created:   2019-08-18 03:44:11                  |
| modified:   2019-08-18 03:44:11                  |
|     done:   N/A                                  |
| duration:   0:00:00                              |
+--------------------------------------------------+

NOTES:
Just a test note.\
"""

@pytest.fixture
def test_data():
    from senile import todo
    for task in todo.Task.find([]):
        task.remove()
    for d in TASK_DATA:
        t = todo.Task()
        t.modify(d.split(" "))
        t.save()
    t = todo.Task.load("2")
    t.notes = TASK_2_NOTE
    t.save()
    return

###
# Tests for __main__.py
###

def test__main__stop_all_tasks(test_data):
    m.stop_all_tasks()
    tasks = todo.Task.find([])
    for t in tasks:
        assert t.status != todo.status_desc['active']

###
# Tests for todo.py
###

def test_is_int():
    assert todo.is_int(-1)
    assert todo.is_int(0)
    assert todo.is_int(1)
    assert todo.is_int("-1")
    assert todo.is_int("0")
    assert todo.is_int("1")
    assert todo.is_int(1.2)
    assert todo.is_int(True)
    assert todo.is_int(False)
    assert not todo.is_int(".2")
    assert not todo.is_int("2a")

def test_get_tags_count(test_data):
    tags_count = todo.get_tags_count()
    for a, b in zip(tags_count, TAGS_COUNT):
        assert a[0] == b[0]
        assert a[1] == b[1]

def test_normalize(test_data):
    todo.normalize()
    #TODO

def test_task_table(test_data):
    task_table = todo.task_table(todo.Task.find([]))
    for a, b in zip(task_table.split("\n"), TASK_TABLE.split("\n")):
        assert a[:5] == b[:5]
        assert a[13:50] == b[13:50]
        assert a[57:] == b[57:]

def test_Task__eiq__():
    t1 = todo.Task()
    t2 = todo.Task()
    assert t1 != t2
    t1.uuid = t2.uuid
    assert t1 == t2

def test_Task_parse():
    data = todo.Task.parse(
            "one +two %3 four -five @active +six seven @eight %5".split(" "))
    assert data["include_tags"] == set(["two", "six"])
    assert data["exclude_tags"] == set(["five"])
    assert data["words"] == ["one", "four", "seven", "@eight"]
    assert data["priority"] == 5
    assert data["status"] == [2]
    data = todo.Task.parse(
            "%22 test +test @done %test %12 @hidden".split(" "))
    assert data["include_tags"] == set(["test"])
    assert data["exclude_tags"] == set()
    assert data["words"] == ["test", "%test"]
    assert data["priority"] == 22
    assert data["status"] == [0,-1]

def test_Task_find(test_data):
    data = []
    tasks = todo.Task.find(data)
    assert len(tasks) == 14
    data = "+tag_a -tag_b".split(" ")
    tasks = todo.Task.find(data)
    assert len(tasks) == 4
    data = "-tag_b %17 +tag_a %2".split(" ")
    tasks = todo.Task.find(data)
    assert len(tasks) == 2
    data = "-tag_b %18 +tag_a %2".split(" ")
    tasks = todo.Task.find(data)
    assert len(tasks) == 1
    assert tasks[0].text == "task 8"
    data = "task 2".split(" ")
    tasks = todo.Task.find(data)
    assert len(tasks) == 14
    data = "task 2 @hidden".split(" ")
    tasks = todo.Task.find(data)
    assert len(tasks) == 1
    assert tasks[0].text == "task 12"

def test_Task_modify():
    options = ["new %77", "text +tag_tag @hidden"]
    task = todo.Task()
    task.modify(options)
    assert task.text == "new text"
    assert task.tags == set(["tag_tag"])
    assert task.priority == 77
    assert task.status == -1
    task.modify(["@todo"])
    task.modify(["@todo"])
    task.modify(["@active"])
    task.modify(["@active"])
    task.modify(["@done"])
    task.modify(["@done"])
    task.modify(["@hidden"])
    task.modify(["@hidden"])
    task.modify(["@bad"])

def test_Task_hide():
    status = -1
    task = todo.Task()
    assert task.status != status
    task.hide()
    assert task.status == status
    task.hide()
    assert task.status == status

def test_Task_done():
    status = 0
    task = todo.Task()
    assert task.status != status
    task.done()
    assert task.status == status
    task.done()
    assert task.status == status

def test_Task_todo():
    status = 1
    task = todo.Task()
    task.start()
    assert task.status != status
    task.todo()
    assert task.status == status
    task.todo()
    assert task.status == status

def test_Task_start():
    status = 2
    task = todo.Task()
    assert task.status != status
    task.start()
    assert task.status == status
    task.start()
    assert task.status == status

def test_Task_stop():
    status = 1
    task = todo.Task()
    task.hide()
    assert task.status != status
    task.stop()
    assert task.status != status
    task.start()
    task.stop()
    assert task.status == status

def test_Task_calc_duration():
    task = todo.Task()
    task.start()
    time.sleep(5.5)
    task.done()
    assert task.calc_duration() == "0:00:05"

def test_Task_get_status_icon():
    task = todo.Task()
    task.hide()
    assert task.get_status_icon() == "👻"
    task.todo()
    assert task.get_status_icon() == "⭕"
    task.start()
    assert task.get_status_icon() == "🔴"
    task.done()
    assert task.get_status_icon() == "✔️ "

def test_Task_get_status_text():
    task = todo.Task()
    task.hide()
    assert task.get_status_text() == "hidden"
    task.todo()
    assert task.get_status_text() == "todo"
    task.start()
    assert task.get_status_text() == "active"
    task.done()
    assert task.get_status_text() == "done"

def test_Task___str__(test_data):
    task = todo.Task.load("12")
    task_string = "12 | "+task.uuid[:8]+" | task 13 more text | 44 | 🔴"
    assert task_string == str(task)

def test_Task_info(test_data):
    task = todo.Task.load("2")
    info = task.info()
    for i in 0,1,3,4,5,8,9,10,11,12,13:
        assert TASK_2_INFO.split("\n")[i] == info.split("\n")[i]
    task.done()
    info = task.info()
    for i in 0,1,3,4,10,11,12,13:
        assert TASK_2_INFO.split("\n")[i] == info.split("\n")[i]
    assert info.split("\n")[5] == "|   status:   done ✔️                               |"
    #TODO: check lines with time

def test_Task_load(test_data):
    task = todo.Task.load("12")
    assert task.text == "task 13 more text"
    uuid = task.uuid
    task = todo.Task.load(uuid)
    assert task.text == "task 13 more text"
    task = todo.Task.load("test")
    assert task == None

def test_Task_save():
    task_1 = todo.Task()
    task_1.modify("%12 test +tag_1 +tag_2 @done")
    task_1.save()
    task_2 = todo.Task.load(task_1.uuid)
    assert task_1 == task_2
    task = todo.Task()
    task.save()

def test_Task_remove():
    task_1 = todo.Task()
    task_1.modify("%12 test +tag_1 +tag_2 @done")
    task_1.save()
    task_1.remove()
    task_2 = todo.Task.load(task_1.uuid)
    assert task_2 == None

###
# Test for database.py
###

# Covered by previous tests


